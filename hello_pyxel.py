import pyxel


x = 0
y = 1
tilesize = 8

letter={
  "a":[1 * tilesize ,0],
  "b":[2 * tilesize ,0],
  "c":[3 * tilesize ,0],
  "d":[4 * tilesize ,0],
  "e":[5 * tilesize ,0],
  "f":[6 * tilesize ,0],
  "g":[7 * tilesize ,0],
  "h":[8 * tilesize ,0],
  "i":[9 * tilesize ,0],
  "j":[10 * tilesize ,0],
  "k":[11 * tilesize ,0],
  "l":[12 * tilesize ,0],
  "m":[13 * tilesize ,0],
  "n":[14 * tilesize ,0],
  "o":[15 * tilesize ,0],
  "p":[16 * tilesize ,0],
  "q":[17 * tilesize ,0],
  "r":[18 * tilesize ,0],
  "s":[19 * tilesize ,0],
  "t":[20 * tilesize ,0],
  "u":[21 * tilesize ,0],
  "v":[22 * tilesize ,0],
  "w":[23 * tilesize ,0],
  "x":[24 * tilesize ,0],
  "y":[25 * tilesize ,0],
  "z":[26 * tilesize ,0],
}

def txt(posx,posy,which):
    pyxel.blt(
        x=posx * tilesize,
        y=posy * tilesize,
        img=0,
        u=letter[which][x],
        v=letter[which][y],
        w=tilesize,
        h=tilesize,
        colkey=0,
    )

def txts(posx, posy, sentence):
    for i,c in enumerate(sentence):
        txt(posx+i, posy, c)

class App:
    def __init__(self):
        pyxel.init(160, 120, title="Hello Pyxel")
        pyxel.load("hellopyxel.pyxres")
        pyxel.run(self.update, self.draw)

    def update(self):
        if pyxel.btnp(pyxel.KEY_Q):
            pyxel.quit()

    def draw(self):
        tilesize=8
        pyxel.cls(0)
        pyxel.play(0,1)
        word_start=7
        txt(word_start+0,6,"h")
        txt(word_start+1,6,"e")
        txt(word_start+2,6,"l")
        txt(word_start+3,6,"l")
        txt(word_start+4,6,"o")
        txts(8,8,"pyxel")

App()
